# Comic App - Characters

## Important

### Environment variables

Set the environment variables into .env file at root folder to run on DEV environment
Currently hided on .gitignore to prevent show at public Marvel API Keys
For DEV or any local's environment you have to create a the following .env file:

```
REACT_APP_PUBLIC_KEY={Your public key without quotes}
REACT_APP_PRIVATE_KEY={Your private key without quotes}
```

## Libraries implemented

### axios

In order to have a quick, light and specific library for AJAX request that be tested about cross browser polyfill, I needed to install to save time this minimal package.

### crypto

I have installed crypto library in order to send hash value to get authorization to request Marvel API's services.

### redux-thunk

For handle API calls and control request's state I have to implement this library.
    : "^3.4.0",


### react-router-dom

I installed this package to handle Routes links and control path changes. It was a little hard to handle react-router v4 because my old knoledge of v3 is no longer retrocompatible.

### bootstrap 3.x

I implement bootstrap in order to style the app although is not a requirement, but I need to do a quick UI to focus in others tasks.
