import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import NotFoundView from './views/NotFoundView/NotFoundView';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import thunk from 'redux-thunk';
import actionTypes from './constants/actionTypes';
import expect from 'expect';
configure({ adapter: new Adapter() });

const middlewares = [thunk];

// Components
describe('App Component', () => {
    it('app complete render without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});

// Views
describe('NotFoundView', () => {
    it('should render an error message', () => {
        const wrapper = shallow(<NotFoundView />);
        expect(wrapper.text()).toEqual('404 Page Not Found');
    });
});

// Reducers
import global from './reducers/global';

describe('global Reducer', () => {
    const initialState = {
        characters: [],
        count: 0,
        offset: 0,
        total: 0,
        copyright: '',
        isFetching: false,
        isFetched: false,
        isError: false,
    };

    it('returns the initial state when an action type not matched', () => {
        const reducer = global(undefined, {});
        expect(reducer).toEqual(initialState);
    });

    it('handles FETCH_CHARACTERS_REQUEST as expected', () => {
        const reducer = global(initialState, { type: actionTypes.FETCH_CHARACTERS_REQUEST });

        expect(reducer).toEqual({
            characters: [],
            copyright: '',
            count: 0,
            isError: false,
            isFetched: false,
            isFetching: true,
            offset: 0,
            total: 0,
        });
    });

    it('handles FETCH_CHARACTERS_SUCCESS as expected', () => {
        const reducer = global(initialState, {
            type: actionTypes.FETCH_CHARACTERS_SUCCESS,
            payload: {
                characters: [
                    {
                        id: 1111121,
                        name: 'foo',
                        thumbnail: {
                            path: 'bar',
                            extension: 'xyz',
                        },
                        comics: {
                            available: 1,
                        },
                        series: {
                            available: 0,
                        },
                        events: {
                            available: 1,
                        },
                        stories: {
                            available: 1,
                        },
                    },
                ],
                copyright: 'foo',
                count: 1,
                offset: 2,
                total: 3,
            },
        });

        expect(reducer).toEqual({
            characters: [
                {
                    id: 1111121,
                    name: 'foo',
                    thumbnail: 'bar.xyz',
                    hasComics: true,
                    hasSeries: false,
                    hasEvents: true,
                    hasStories: true,
                },
            ],
            copyright: 'foo',
            count: 1,
            offset: 2,
            total: 3,
            isFetching: false,
            isFetched: true,
            isError: false,
        });
    });

    it('handles FETCH_CHARACTERS_FAILURE as expected', () => {
        const reducer = global(initialState, { type: actionTypes.FETCH_CHARACTERS_FAILURE });

        expect(reducer).toEqual({
            characters: [],
            copyright: '',
            count: 0,
            isError: true,
            isFetched: false,
            isFetching: false,
            offset: 0,
            total: 0,
        });
    });
});

import character from './reducers/character';

describe('character Reducer', () => {
    const initialState = {
        id: '',
        name: '',
        thumbnail: '',
        biography: '',
        urls: {
            details: '',
            wiki: '',
            comic: '',
        },
        isFetching: false,
        isFetched: false,
        isError: false,
    };

    it('returns the initial state when an action type not matched', () => {
        const reducer = character(undefined, {});
        expect(reducer).toEqual(initialState);
    });

    it('handles FETCH_CHARACTER_INFO_REQUEST as expected', () => {
        const reducer = character(initialState, { type: actionTypes.FETCH_CHARACTER_INFO_REQUEST });

        expect(reducer).toEqual({
            id: '',
            name: '',
            thumbnail: '',
            biography: '',
            urls: {
                details: '',
                wiki: '',
                comic: '',
            },
            isFetching: true,
            isFetched: false,
            isError: false,
        });
    });

    it('handles FETCH_CHARACTER_INFO_SUCCESS as expected', () => {
        const reducer = character(initialState, {
            type: actionTypes.FETCH_CHARACTER_INFO_SUCCESS,
            payload: {
                characterInfo: {
                    id: 1000002,
                    name: 'baz',
                    description: 'lorem ipsum',
                    thumbnail: {
                        path: 'http://test/img/111',
                        extension: 'jpg',
                    },
                    comics: {
                        available: 1,
                    },
                    series: {
                        available: 1,
                    },
                    stories: {
                        available: 1,
                    },
                    events: {
                        available: 1,
                    },
                    urls: [
                        {
                            type: 'detail',
                            url: 'http://detail/url',
                        },
                        {
                            type: 'wiki',
                            url: 'http://wiki/url',
                        },
                        {
                            type: 'comiclink',
                            url: 'http://comiclink/url',
                        },
                    ],
                },
            },
        });

        expect(reducer).toEqual({
            id: 1000002,
            name: 'baz',
            thumbnail: 'http://test/img/111.jpg',
            biography: 'lorem ipsum',
            urls: {
                details: 'http://detail/url',
                wiki: 'http://wiki/url',
                comic: 'http://comiclink/url',
            },
            isFetching: false,
            isFetched: true,
            isError: false,
        });
    });

    it('handles FETCH_CHARACTER_INFO_FAILURE as expected', () => {
        const reducer = character(initialState, { type: actionTypes.FETCH_CHARACTER_INFO_FAILURE });

        expect(reducer).toEqual({
            id: '',
            name: '',
            thumbnail: '',
            biography: '',
            urls: {
                details: '',
                wiki: '',
                comic: '',
            },
            isFetching: false,
            isFetched: false,
            isError: true,
        });
    });
});
