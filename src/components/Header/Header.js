import './Header.scss';

// Vendor
import React from 'react';

// Asset
import banner from '../../assets/img/banner.jpg';

const Header = () => (
    <header className="header">
        <div className="header__banner">
            <img src={banner} alt="App's banner" className="header__banner-img" />
        </div>
    </header>
);

export default Header;
