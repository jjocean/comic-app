import './Pagination.scss';

// Vendor
import React from 'react';
import PropTypes from 'prop-types';

const Pagination = props => {
    const { modifier, offset, count, total } = props;
    const paginationClassName = `pagination pagination--${modifier}`;
    const nextOffset = offset + count;
    let paginateLeft = null;
    let paginateRight = null;
    if (offset > 0) {
        const prevOffset = offset - count;
        paginateLeft = (
            <div
                data-id={prevOffset}
                className="pagination__arrow btn btn-primary glyphicon glyphicon-chevron-left"
                onClick={props.onPrevPaginationClick}
            />
        );
    }

    if (nextOffset < total) {
        paginateRight = (
            <div
                data-id={nextOffset}
                className="pagination__arrow btn btn-primary glyphicon glyphicon-chevron-right"
                onClick={props.onNextPaginationClick}
            />
        );
    }

    return (
        <div className={paginationClassName}>
            {paginateLeft}
            {paginateRight}
        </div>
    );
};

Pagination.propTypes = {
    modifier: PropTypes.oneOf(['top', 'bottom']).isRequired,
    offset: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
    onPrevPaginationClick: PropTypes.func.isRequired,
    onNextPaginationClick: PropTypes.func.isRequired,
};

export default Pagination;
