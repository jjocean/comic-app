import './Footer.scss';

// Vendor
import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { FACEBOOK_URL, TWITTER_URL, GOOGLE_PLUS_URL } from '../../constants/index';

const Footer = ({ copyright }) => (
    <footer className="footer">
        <h5 className="footer__copyright">{copyright}</h5>
        <ul className="list-inline">
            <li>
                <a
                    href={FACEBOOK_URL}
                    className="btn btn-social-icon btn-facebook"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <i className="footer__icon fa fa-facebook" />
                </a>
            </li>
            <li>
                <a
                    href={TWITTER_URL}
                    className="btn btn-social-icon btn-twitter"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <i className="footer__icon fa fa-twitter" />
                </a>
            </li>
            <li>
                <a
                    href={GOOGLE_PLUS_URL}
                    className="btn btn-social-icon btn-google-plus"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <i className="footer__icon fa fa-google-plus" />
                </a>
            </li>
        </ul>
    </footer>
);

Footer.propTypes = {
    copyright: PropTypes.string.isRequired,
};

export default Footer;
