import './CharacterItem.scss';

// Vendor
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// Components
import Tag from '../../components/Tag/Tag';

// Helpers
import { getReferences } from '../../helpers/characterHelper';

// The idea is start to use functional components where's possible
function buildTagList(references) {
    const tags = references.map(reference => (
        <div key={reference} className="character-item__tag-item">
            <Tag key={reference} label={reference} />
        </div>
    ));

    return (
        <div className="character-item__tag-list">
            {tags}
        </div>
    );
}

const CharacterItem = ({ character }) => {
    const { id, name, thumbnail } = character;
    const learnMoreLink = `/character/${id}`; // TODO: Move this link to the constants file
    const references = getReferences(character);
    const tagList = references.length > 0 ? buildTagList(references) : null;

    return (
        <div className="character-item">
            <div className="character-item__thumbnail thumbnail">
                <div className="character-item__img-wrapper">
                    <img className="character-item__crop-img group list-group-image" src={thumbnail} alt={name} />
                </div>
                <div className="caption">
                    <div className="character-item__heading-group">
                        <h4 className="character-item__title group inner list-group-item-heading">{name}</h4>
                    </div>
                    {tagList}
                    <div className="character-item__caption-footer">
                        <Link to={learnMoreLink} className="character-item__link">
                            Learn More
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

CharacterItem.propTypes = {
    character: PropTypes.object.isRequired,
};

export default CharacterItem;
