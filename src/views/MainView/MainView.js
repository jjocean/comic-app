import './MainView.scss';

// Vendor
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import Pagination from '../../components/Pagination/Pagination';
import CharacterItem from '../../components/CharacterItem/CharacterItem';

// Actions
import globalActions from '../../actions/global';

class MainView extends Component {
    constructor(props) {
        super(props);

        this.handlePrevPaginationClick = this.handlePrevPaginationClick.bind(this);
        this.handleNextPaginationClick = this.handleNextPaginationClick.bind(this);
    }

    handlePrevPaginationClick(ev) {
        const nextOffset = Number(ev.currentTarget.dataset.id);
        this.props.fetchCharacters(nextOffset);
    }

    handleNextPaginationClick(ev) {
        const nextOffset = Number(ev.currentTarget.dataset.id);
        this.props.fetchCharacters(nextOffset);
    }

    buildPagination(modifier, offset, count, total) {
        return (
            <Pagination
                modifier={modifier}
                offset={offset}
                count={count}
                total={total}
                onPrevPaginationClick={this.handlePrevPaginationClick}
                onNextPaginationClick={this.handleNextPaginationClick}
            />
        );
    }

    buildCharacterList(characters) {
        const list = characters.map(character => (
            <div key={character.id} className="item col-xs-12 col-md-6 col-lg-3">
                <CharacterItem character={character} />
            </div>
        ));

        return list;
    }

    render() {
        const { immGlobal } = this.props;
        const { characters, offset, count, total, isFetched } = immGlobal;
        const characterList = this.buildCharacterList(characters);
        let paginationTop = null;
        let paginationBottom = null;

        if (isFetched && count > 0) {
            paginationTop = this.buildPagination('top', offset, count, total);
            paginationBottom = this.buildPagination('bottom', offset, count, total);
        }

        return (
            <main className="main-view">
                <section>
                    <h2 className="main-view__header">Character's Main Section</h2>
                    {paginationTop}
                    <div className="main-view__list-group">
                        <div className="row list-group">{characterList}</div>
                    </div>
                    {paginationBottom}
                </section>
            </main>
        );
    }
}

MainView.propTypes = {
    immGlobal: PropTypes.object.isRequired,
};

export default (MainView = connect(
    state => ({
        immGlobal: state.global,
    }),
    {
        fetchCharacters: globalActions.fetchCharacters,
    }
)(MainView));
