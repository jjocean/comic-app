import React from 'react';

const NotFoundView = () => (
    <h1>404 Page Not Found</h1>
);

export default NotFoundView;
