import './CharacterView.scss';

// Vendor
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// Actions
import characterActions from '../../actions/character';

class CharacterView extends Component {
    componentDidMount() {
        const { match } = this.props;
        const { id } = match.params;
        this.props.fetchCharacterInfo(id);
    }

    render() {
        const { immCharacter } = this.props;
        const { name, thumbnail, biography, urls } = immCharacter;

        return (
            <section className="character-view">
                <div className="character-view__content">
                    <div className="character-view__content-left">
                        <div className="character-view__header">{name}</div>
                        <div className="character-view__body">
                            <p className="character-view__info">{biography}</p>
                        </div>
                        <div className="character-view__list-items">
                            <a href={urls.details} className="character-view__link-button btn btn-danger">
                                Read more
                            </a>
                            <a href={urls.wiki} className="character-view__link-button btn btn-success">
                                Wiki
                            </a>
                            <a href={urls.comic} className="character-view__link-button btn btn-primary">
                                Related comics
                            </a>
                        </div>
                    </div>
                    <div className="character-view__content-right">
                        <div className="character-view__thumbnail-wrapper">
                            <img src={thumbnail} alt={name} className="character-view__img-thumbnail" />
                        </div>
                    </div>
                    <div className="character-view__clear" />
                </div>
                <div className="character-view__footer">
                    <Link to="/" className="character-view__link-button btn btn-warning">Home</Link>
                </div>
            </section>
        );
    }
}

CharacterView.propTypes = {
    immCharacter: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

export default (CharacterView = connect(
    state => ({
        immCharacter: state.character,
    }),
    {
        fetchCharacterInfo: characterActions.fetchCharacterInfo,
    }
)(CharacterView));
