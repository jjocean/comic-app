// Vendors
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Views
import MainView from './MainView/MainView';
import CharacterView from './CharacterView/CharacterView';
import NotFoundView from './NotFoundView/NotFoundView';

class Index extends Component {
    // @TODO: consider pass this Index to functional
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={MainView} />
                    <Route exact path="/character/:id" component={CharacterView} />
                    <Route component={NotFoundView} />
                </Switch>
            </Router>
        );
    }
}

export default Index;
