import './GlobalView.scss';

// Vendor
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Index from '../index';

// Actions
import globalActions from '../../actions/global';

class GlobalView extends Component {
    componentDidMount() {
        this.props.fetchCharacters();
    }

    render() {
        const { immGlobal } = this.props;
        const { isFetching, copyright } = immGlobal;
        const main = isFetching ? <div className="loading" /> : <Index />

        return (
            <div className="global-view__wrapper">
                <div className="global-view__header">
                    <Header />
                </div>
                <main className="global-view__main">
                    {main}
                </main>
                <div className="global-view__footer">
                    <Footer copyright={copyright} />
                </div>
            </div>
        );
    }
}

GlobalView.propTypes = {
    immGlobal: PropTypes.object.isRequired,
    fetchCharacters: PropTypes.func.isRequired,
};

export default GlobalView = connect(
    state => ({
        immGlobal: state.global,
    }),
    {
        fetchCharacters: globalActions.fetchCharacters,
    }
)(GlobalView);
