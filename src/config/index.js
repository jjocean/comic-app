import crypto from 'crypto';

// Set the environment variables into .env file at root folder to run on DEV environment
// Currently hided on .gitignore to prevent show at public Marvel API Keys
// For DEV or any local's environment you have to create a the following .env file:

// REACT_APP_PUBLIC_KEY={Your public key without quotes}
// REACT_APP_PRIVATE_KEY={Your private key without quotes}

const PUBLIC_KEY = process.env.REACT_APP_PUBLIC_KEY || '';
const PRIVATE_KEY = process.env.REACT_APP_PRIVATE_KEY || '';
const ts = new Date().getTime();
const hash = crypto
    .createHash('md5')
    .update(ts + PRIVATE_KEY + PUBLIC_KEY)
    .digest('hex');

const authorization = {
    apikey: PUBLIC_KEY,
    ts,
    hash,
};

export {
    authorization,
};
