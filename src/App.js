import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import GlobalView from './views/GlobalView/GlobalView';

const App = () => (
    <Provider store={store}>
        <GlobalView />
    </Provider>
);

export default App;
