import actionTypes from '../constants/actionTypes';

const initialState = {
    characters: [],
    count: 0,
    offset: 0,
    total: 0,
    copyright: '',
    isFetching: false,
    isFetched: false,
    isError: false,
};

const parseCharacters = characters => {
    const parsedCharacters = characters.map(character => {
        const {
            id,
            name,
            comics,
            series,
            events,
            stories,
            thumbnail: { path, extension },
        } = character;

        const thumbnail = `${path}.${extension}`;
        const hasComics = comics.available > 0;
        const hasSeries = series.available > 0;
        const hasEvents = events.available > 0;
        const hasStories = stories.available > 0;
        const model = {
            id,
            name,
            thumbnail,
            hasComics,
            hasSeries,
            hasEvents,
            hasStories,
        };

        return model;
    });

    return parsedCharacters;
};

const globalReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CHARACTERS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true,
                isFetched: false,
                isError: false,
                characters: [],
            });
        case actionTypes.FETCH_CHARACTERS_SUCCESS:
            const { characters, copyright, count, offset, total } = action.payload;
            const parsedCharacters = parseCharacters(characters);

            return Object.assign({}, state, {
                characters: parsedCharacters,
                copyright,
                count,
                offset,
                total,
                isFetched: true,
                isFetching: false,
                isError: false,
            });
        case actionTypes.FETCH_CHARACTERS_FAILURE:
            return Object.assign({}, state, {
                isError: true,
                isFetched: false,
                isFetching: false,
                characters: [],
            });
        default:
            return state;
    }
};

export default globalReducer;
