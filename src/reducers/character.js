import { get } from 'lodash';
import actionTypes from '../constants/actionTypes';

const initialState = {
    id: '',
    name: '',
    thumbnail: '',
    biography: '',
    urls: {
        details: '',
        wiki: '',
        comic: '',
    },
    isFetching: false,
    isFetched: false,
    isError: false,
};

const parseCharacterInfo = character => {
    const {
        id,
        name,
        thumbnail: { path, extension },
        description,
        urls,
    } = character;

    const thumbnail = `${path}.${extension}`;
    const detailsUrl = get(urls, '[0].url', '');
    const wikiUrl = get(urls, '[1].url', '');
    const comicUrl = get(urls, '[2].url', '');

    const model = {
        id,
        name,
        thumbnail,
        biography: description,
        urls: {
            details: detailsUrl,
            wiki: wikiUrl,
            comic: comicUrl,
        },
    };

    return model;
};

const characterReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CHARACTER_INFO_REQUEST:
            return Object.assign({}, state, {
                isFetching: true,
                isFetched: false,
                isError: false,
                character: initialState.character,
            });
        case actionTypes.FETCH_CHARACTER_INFO_SUCCESS:
            const { characterInfo } = action.payload;
            const parsedCharacterInfo = parseCharacterInfo(characterInfo);

            return Object.assign({}, state, {
                ...parsedCharacterInfo,
                isFetched: true,
                isFetching: false,
                isError: false,
            });
        case actionTypes.FETCH_CHARACTER_INFO_FAILURE:
            return Object.assign({}, state, {
                isError: true,
                isFetched: false,
                isFetching: false,
                character: initialState.character,
            });
        default:
            return state;
    }
};

export default characterReducer;
