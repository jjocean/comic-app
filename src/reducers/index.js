import { combineReducers } from 'redux';
import global from './global';
import character from './character';

const root = combineReducers({
    character,
    global
});

export default root;
