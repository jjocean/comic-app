// Vendor
import axios from 'axios';
import { get } from 'lodash';

// Config
import { authorization } from '../config';

// Constants
import actionTypes from '../constants/actionTypes';

const fetchCharacterInfoRequest = () => ({
    type: actionTypes.FETCH_CHARACTER_INFO_REQUEST,
});

const fetchCharacterInfoSuccess = characterInfo => ({
    type: actionTypes.FETCH_CHARACTER_INFO_SUCCESS,
    payload: { characterInfo },
});

const fetchCharacterInfoFailure = error => ({
    type: actionTypes.FETCH_CHARACTER_INFO_FAILURE,
    payload: { error },
});

const fetchCharacterInfo = id => (dispatch, getState) => {
    const url = `http://gateway.marvel.com/v1/public/characters/${id}`;
    const params = {
        ...authorization,
    };

    dispatch(fetchCharacterInfoRequest());
    axios
        .get(url, { params })
        .then(response => {
            const character = get(response, 'data.data.results[0]', {}); // Fallback when there is not results
            dispatch(fetchCharacterInfoSuccess(character));
        })
        .catch(error => {
            dispatch(fetchCharacterInfoFailure(error));
        });
};

export default {
    fetchCharacterInfo,
};
