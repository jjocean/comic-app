// Vendor
import axios from 'axios';
import { get } from 'lodash';

// Config
import { authorization } from '../config';

// Constants
import actionTypes from '../constants/actionTypes';

const fetchCharactersRequest = () => ({
    type: actionTypes.FETCH_CHARACTERS_REQUEST,
});

const fetchCharactersSuccess = (characters, count, offset, total, copyright) => ({
    type: actionTypes.FETCH_CHARACTERS_SUCCESS,
    payload: {
        characters,
        copyright,
        count,
        offset,
        total,
    },
});

const fetchCharactersFailure = error => ({
    type: actionTypes.FETCH_CHARACTERS_FAILURE,
    payload: { error },
});

const fetchCharacters = (offset = 0) => (dispatch, getState) => {
    const url = 'http://gateway.marvel.com/v1/public/characters';

    const params = {
        ...authorization,
        limit: 8, // This limit is only for easy test on pagination
        offset,
    };

    dispatch(fetchCharactersRequest());
    return axios
        .get(url, { params })
        .then(response => {
            const characters = get(response, 'data.data.results', []); // Fallback when there is not results
            const count = get(response, 'data.data.count');
            const offset = get(response, 'data.data.offset');
            const total = get(response, 'data.data.total');
            const copyright = get(response, 'data.attributionText');
            dispatch(fetchCharactersSuccess(characters, count, offset, total, copyright));
        })
        .catch(error => {
            dispatch(fetchCharactersFailure(error));
        });
};

export default {
    fetchCharacters,
};
