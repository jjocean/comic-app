const getReferences = (character = {}) => {
    const { hasComics, hasSeries, hasEvents, hasStories } = character;
    const references = [];

    if (hasComics) {
        references.push('Comics');
    }

    if (hasSeries) {
        references.push('Series');
    }

    if (hasEvents) {
        references.push('Events');
    }

    if (hasStories) {
        references.push('Stories');
    }

    return references;
};

export { getReferences };
